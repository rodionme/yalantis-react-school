# Test project for Yalantis React School


## Main task: 
GET: `https://yalantis-react-school-api.yalantis.com/api/task0/users` returns an array of users. Each user has the following fields: `id`, `firstName`, `lastName`, `dob` (date of birth). 

Implement the following:
1) fetch users
2) display the list of months
3) highlight the months depending on the number of people born in that month.
4) when hovering over the selected month - display a list of people born in that month.

Condition for the number of people to highlight the months:<br />
[0-2] - gray<br />
[3-6] - blue<br />
[7-10] - green<br />
[11+] - red<br />


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
