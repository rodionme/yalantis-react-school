import React from 'react';
import PropTypes from 'prop-types';

import Month from '../Month';
import './months.css';

const Months = ({ users, onMonthMouseEnter }) => {
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  return (
    <div className="months">
      <ul className="months__list list-group list-group-flush">
        {monthNames.map((monthName, idx) => {
          const monthCapacity = users[idx] && users[idx].length;

          return (
            <li
              key={idx}
              onMouseEnter={() => onMonthMouseEnter(idx)}
              className="months__item list-group-item"
            >
              <Month monthName={monthName} monthCapacity={monthCapacity} />
            </li>
          );
        })}
      </ul>
    </div>
  );
};

Months.propTypes = {
  users: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.object)).isRequired,
  onMonthMouseEnter: PropTypes.func.isRequired,
};

export default Months;
