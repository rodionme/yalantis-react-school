import React from 'react';
import PropTypes from 'prop-types';

const User = ({ user }) => {
  const { firstName, lastName, dob } = user;

  return (
    <div className="user">
      <span className="user__name">
        {firstName} {lastName}
      </span>
      <span className="user__dob float-right">{dob}</span>
    </div>
  );
};

User.propTypes = {
  user: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    dob: PropTypes.string.isRequired,
  }),
};

export default User;
