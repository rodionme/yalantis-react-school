import React from 'react';

import UserService from '../../services/user.service';
import Months from '../Months';
import Users from '../Users';
import ErrorMessage from '../ErrorMessage';

class App extends React.Component {
  state = {
    users: [],
    usersByMonth: [],
    error: null,
  };

  updateUsersByMonth = (idx) => {
    this.setState({
      usersByMonth: this.state.users[idx],
    });
  };

  componentDidMount() {
    const userService = new UserService();

    userService
      .getUsersByMonths()
      .then((users) => {
        this.setState({ users });
      })
      .catch(({ message }) => {
        this.setState({ error: message });
      });
  }

  render() {
    if (this.state.error) return <ErrorMessage error={this.state.error} />;

    return (
      <div className="app container p-3">
        <div className="row">
          <section className="app__months col-4">
            <Months users={this.state.users} onMonthMouseEnter={this.updateUsersByMonth} />
          </section>

          <section className="app__user col-8">
            <Users users={this.state.usersByMonth} />
          </section>
        </div>
      </div>
    );
  }
}

export default App;
