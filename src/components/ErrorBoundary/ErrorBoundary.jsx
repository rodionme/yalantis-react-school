import React from 'react';
import ErrorMessage from '../ErrorMessage';

class ErrorBoundary extends React.Component {
  state = { error: null };

  static getDerivedStateFromError(error) {
    return { error };
  }

  render() {
    if (this.state.error) return <ErrorMessage />;

    return this.props.children;
  }
}

export default ErrorBoundary;
