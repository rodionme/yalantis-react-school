import React from 'react';
import PropTypes from 'prop-types';
import './month.css';

const Month = ({ monthName, monthCapacity }) => {
  const getMonthCapacityLevel = (capacity) => {
    switch (true) {
      case capacity <= 2:
        return '1';

      case capacity <= 6:
        return '2';

      case capacity <= 10:
        return '3';

      case capacity >= 11:
        return '4';

      default:
        return '0';
    }
  };

  const monthCapacityLevel = getMonthCapacityLevel(monthCapacity);

  return (
    <div
      className={`month month--level-${monthCapacityLevel} d-flex justify-content-between align-items-center`}
    >
      {monthName} <span className="badge badge-light">{monthCapacity}</span>
      <span className="sr-only">users</span>
    </div>
  );
};

Month.propTypes = {
  monthName: PropTypes.string.isRequired,
  monthCapacity: PropTypes.number,
};

export default Month;
