import React from 'react';
import PropTypes from 'prop-types';
import User from '../User';

const Users = ({ users }) => {
  return (
    <div className="users">
      <ul className="users__list list-group list-group-flush">
        {users.map((user, idx) => (
          <li key={idx} className="users__item list-group-item">
            <User user={user} />
          </li>
        ))}
      </ul>
    </div>
  );
};

Users.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Users;
