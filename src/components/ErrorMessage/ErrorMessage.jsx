import React from 'react';
import PropTypes from 'prop-types';

const ErrorMessage = ({ error }) => {
  return (
    <div className="error p-5">
      <span>You've got an error: {error}</span>
    </div>
  );
};

ErrorMessage.propTypes = {
  error: PropTypes.string.isRequired,
};

export default ErrorMessage;
