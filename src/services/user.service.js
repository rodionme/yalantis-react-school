class UserService {
  getUsers = async () => {
    const usersUrl = 'https://yalantis-react-school-api.yalantis.com/api/task0/users';

    try {
      const response = await fetch(usersUrl);

      return response.json();
    } catch (err) {
      throw new Error(`Could not fetch ${usersUrl}`);
    }
  };

  getUsersByMonths = async () => {
    const users = await this.getUsers();
    const usersByMonths = [];

    users.forEach((user) => {
      const dateOfBirth = new Date(user.dob);
      const monthIndex = dateOfBirth.getMonth();

      if (!usersByMonths[monthIndex]) usersByMonths[monthIndex] = [];

      user.dob = dateOfBirth.toLocaleDateString();

      usersByMonths[monthIndex].push(user);
    });

    return usersByMonths;
  };
}

export default UserService;
